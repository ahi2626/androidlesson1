package it.source.myapplication;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Object v;
    private boolean counter;
    private View buttonSetMinus;
    private View buttonSetPlus;
    private EditText textCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        TextView text = findViewById(R.id.text);
//        text.setText("Hello SourceIT");


        final TextView textWorld = findViewById(R.id.textWorld);

        Button buttonSetPrint = findViewById(R.id.buttonSetPrint);
        Button buttonSetDelete = findViewById(R.id.buttonSetDelete);

        buttonSetPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textWorld.setText("Привет Мир");
            }
        });

        buttonSetDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textWorld.setText("");
            }
        });


        //Task2
        final TextView textCounter = findViewById(R.id.textCounter);
        Button buttonSetMinus = findViewById(R.id.buttonSetMinus);
        Button buttonSetPlus = findViewById(R.id.buttonSetPlus);

        final String[] counter = {"1"};
        textCounter.setText(counter[0]);


        buttonSetMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = Integer.parseInt(counter[0]) - 1;
                counter[0] = Integer.toString(i);
                textCounter.setText(counter[0]);
            }
        });


        buttonSetPlus.setOnClickListener(new View.OnClickListener() {
         @Override
            public void onClick(View v) {
                int i = Integer.parseInt(counter[0]) + 1;
                counter[0] = Integer.toString(i);
                textCounter.setText(counter[0]);

            }
        });
 }
}

